make check-staging # Check the staging registry with a login/push/pull
make check-production # Check the production registry with a login/push/pull


To use:

* fork the repository
* generate an api token that has access to the registry
* export REGISTRY_ACCESS_TOKEN to the token value
* export GITLAB_USER to your gitlab username
test
* [some link][]

[some link]: https://gitlab.com/gitlab-com/people-group/hiring-processes/-/tree/master/Engineering/Infrastructure/Platforms/Dedicated
