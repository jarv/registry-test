#!/bin/bash

send_event() {
  MSG="$1"
  TYPE="${2:-configuration}"
  ENV="${3:-gprd}"
  TS=$(date -u +%s000)
  USERNAME="${GITLAB_USER_LOGIN:-unknown}"
  SOURCE="${CI_JOB_URL:-unknown}"
  DATA="
    {
      \"time\": \"$TS\",
      \"type\": \"$TYPE\",
      \"message\": \"$MSG\",
      \"env\": \"$ENV\",
      \"username\": \"$USERNAME\",
      \"source\": \"$SOURCE\"
    }
  "
  echo "Sending event: \"$MSG\""
  curl -s -X POST "$ES_NONPROD_EVENTS_URL/events-$ENV/_doc" -H 'Content-Type: application/json' -d "$DATA" > /dev/null
}

send_event "this is a test message"
